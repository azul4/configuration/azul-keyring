# azul keyring

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Keyring for azul repository

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/configuration/azul-keyring.git
```

<br><br>
**Note from Rafael:**

keys located at: **/usr/share/pacman/keyrings/**

**azul-keyring** installs the authorized gpg keys from azul repository

**azul-trusted** file explained:
```
2F6D8657D79199892002E6465F819C731E28BDAE:4: Rafael
```

<br><br>

**How to get the value indicated above?**

Once we have the gpg key created in our system, it will be enough to execute the following from the terminal:

```
$ gpg --list-secret-keys --keyid-format LONG
/home/user/.gnupg/pubring.kbx
-----------------------------
sec   rsa4096/xxxxxxxxxxxxxxxxxxx 2019-09-05 [SC]
      xxxxxxxxxxxxxxxxxxxxxxxxxxx --> **Send me this number**
uid   ....
ssb   rsa4096/xxxxxxxxxxxxxxxxxxxx
```


Send me the number marked by -->****Send me this number****  and also send me your **gpg public key**.

Your public gpg key must be in the file **azul.gpg** (the format is clearly visible when accessing that file).

How do you send your public pgp key to a file? From the terminal, it is done with:

```
gpg --armor --export >> your_name_for_example.asc
```

Please, send me this file.

Checking what is needed: it is the file with your **public pgp key**, and the value indicated above.

<br>

**You can read about the procedure to create a pgp key here:**

https://www.inmotionhosting.com/support/edu/everything-email/how-to-create-a-gpg-key/

<br><br>
Content of the file **azul-revoked**

```

```

The blue-revoked file is empty, because there is currently no revoked key.

**End note from Rafael**
